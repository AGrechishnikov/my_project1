public class Main {
    public static void main(String[] args) {
        int book_number = 0x21F62;
        System.out.println(book_number);
        long phone_num = 89819978955l;
        System.out.println(phone_num);
        int last_two = 0b110111;
        System.out.println(last_two);
        int last_four = 067;
        System.out.println(last_four);
        int remainderOfDivision = (book_number-1) % 26 + 1;
        System.out.println(remainderOfDivision);
        char symbol = 0b01000110;
        System.out.println(symbol);
    }
}